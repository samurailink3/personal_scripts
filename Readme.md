# Personal Scripts

## Generic Rolling Backup Script

This is a simple rolling (and optionally compressing) backup script that's
generic enough to be used for just about anything with some slight modification.
I don't really use this much anymore and have moved to Restic for most of my
backup needs.

### Usage

Modify the script variables to your liking and run it. This has been tested on
Debian and FreeBSD, so it should be portable enough to use on just about any
\*nix.

### Documentation

The script has been liberally commented at every point. If you want to
know what something does or how to use it, read the script.

### License

MIT License

## Restic

This is a collection of opinionated Restic scripts and helpers.

### Usage

* Copy the env-var example file: `cp restic.env.example restic.env`
    * Edit this file to set your backup options
* Run your backup automation: `/bin/bash restic.bash restic.env`
* Add that to cron and stop worrying about your backups

### Files

* `restic.bash` - All-in-one automation to back things up to a Restic
  repository. Supports local LUKS-encrypted drives and S3-bucket repositories. I
  don't use the local LUKS option anymore, but it should still work. You can
  control all of the script functionality with the `restic.env` variables.
* `restic.env.example` - A copy-pastable example file to set your variables and
  get your backups configured.
* `restic-bin.bash` - Because restic is configured with the env file, it can be
  annoying copy-pasta-ing all of the environment variables in the event you need
  to run `restic` directly. This micro-script sets the environment variables
  from the provided env file, then passes the remaining arguments to the
  `restic` binary. Instead of `restic snapshots`, try:
  `./restic-bin.bash restic.env snapshots`
