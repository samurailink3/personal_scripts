#!/bin/bash

# Check if a filename argument is provided
if [ $# -eq 0 ]; then
    echo "Error: No filename provided"
    echo "Usage: $0 <config>"
    exit 1
fi

# Store the filename argument
config="$1"

# Check if the file exists
if [ ! -f "$config" ]; then
    echo "Error: File '$config' does not exist"
    exit 1
fi

source "$config"

$RESTIC_BIN  -r $RESTIC_REPO -p $RESTIC_PASSWORD_FILE "${@:2}"
