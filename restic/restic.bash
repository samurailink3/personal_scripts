#!/bin/bash

## TODO: Make different meta-main functions for local/s3, use main as a switcher

# Check if a filename argument is provided
if [ $# -eq 0 ]; then
    echo "Error: No filename provided"
    echo "Usage: $0 <config>"
    exit 1
fi

# Store the filename argument
config="$1"

# Check if the file exists
if [ ! -f "$config" ]; then
    echo "Error: File '$config' does not exist"
    exit 1
fi

source "$config"

timestamp_echo() {
    echo "$($DATE_BIN "${DATE_FLAGS[@]}"):" "$@"
}

mount_backup_drive() {
    timestamp_echo 'Mounting drive.'
    $CRYPTSETUP_BIN open $DRIVE_PATH --type luks -d $DRIVE_MOUNT_KEYFILE $DRIVE_MOUNT_CRYPTNAME
    $MOUNT_BIN /dev/mapper/$DRIVE_MOUNT_CRYPTNAME $DRIVE_MOUNT
    timestamp_echo 'Drive mounted.'
}

unmount_backup_drive() {
    timestamp_echo 'Unmounting drive.'
    $UMOUNT_BIN $DRIVE_MOUNT
    $CRYPTSETUP_BIN close $DRIVE_MOUNT_CRYPTNAME
    timestamp_echo 'Drive unmounted.'
}

print_drive_stats() {
    timestamp_echo 'Drive Space:'
    $DF_BIN "${DF_FLAGS[@]}" $DRIVE_MOUNT
}

print_bucket_stats() {
    timestamp_echo 'Bucket Space Used:'
    $AWS_CLI_BIN s3 "${AWS_S3_SPACE_USED_FLAGS[@]}" s3://"$AWS_S3_BUCKET" | tail -2
}

check() {
    timestamp_echo 'Check started.'
    $RESTIC_BIN -r $RESTIC_REPO -p $RESTIC_PASSWORD_FILE --verbose check
    timestamp_echo 'Check finished.'
}

backup() {
    timestamp_echo 'Backup started.'
    $RESTIC_BIN -r "$RESTIC_REPO" -p $RESTIC_PASSWORD_FILE --verbose backup "${RESTIC_BACKUP_TARGETS[@]}"
    timestamp_echo 'Backup finished.'
}

forget() {
    timestamp_echo 'Forget started'
    $RESTIC_BIN -r $RESTIC_REPO -p $RESTIC_PASSWORD_FILE --verbose forget --keep-daily=$RESTIC_KEEP_DAILY --keep-weekly=$RESTIC_KEEP_WEEKLY --keep-monthly=$RESTIC_KEEP_MONTHLY --keep-yearly=$RESTIC_KEEP_YEARLY
    timestamp_echo 'Forget finished'
}

prune() {
    timestamp_echo 'Prune started'
    $RESTIC_BIN -r $RESTIC_REPO -p $RESTIC_PASSWORD_FILE --verbose prune
    timestamp_echo 'Prune finished'
}

main() {
    if [ "$BACKUP_TYPE" == 'Local' ]; then
        if [ -L $DRIVE_PATH ]; then
            timestamp_echo 'Restic Local started.'
            mount_backup_drive
            if [ $RESTIC_PRINT_STATS == 'ENABLED' ]; then
                print_bucket_stats
            fi
            if [ $RESTIC_CHECK_BACKUPS == 'ENABLED' ]; then
                check
            fi
            backup
            if [ $RESTIC_FORGET_BACKUPS == 'ENABLED' ]; then
                forget
                prune
            fi
            unmount_backup_drive
            timestamp_echo 'Restic Local finished.'
            exit 0
        else
            timestamp_echo 'Backup drive not found, skipping backup.'
            exit 0
        fi
    elif [ "$BACKUP_TYPE" == 'S3' ]; then
            timestamp_echo 'Restic S3 started.'
            if [ $RESTIC_PRINT_STATS == 'ENABLED' ]; then
                print_bucket_stats
            fi
            if [ $RESTIC_CHECK_BACKUPS == 'ENABLED' ]; then
                check
            fi
            backup
            if [ $RESTIC_FORGET_BACKUPS == 'ENABLED' ]; then
                forget
                prune
            fi
            timestamp_echo 'Restic S3 finished.'
            exit 0
    else
        timestamp_echo "BACKUP_TYPE is either unset or an invalid value: [$BACKUP_TYPE]"
    fi
}

main >> $LOG_FILE 2>&1

