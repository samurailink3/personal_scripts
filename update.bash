#!/bin/bash

# Get the absolute path of the script
SCRIPT_PATH=$(readlink -f "$0")
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")

# Change to the script's directory
cd "$SCRIPT_DIR" || exit

# Run git pull to self-update
git pull
